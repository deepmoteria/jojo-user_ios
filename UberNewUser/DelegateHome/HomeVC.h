//
//  HomeVC.h
//  Wag
//
//  Created by Elluminati - macbook on 20/09/14.
//  Copyright (c) 2014 Elluminati. All rights reserved.
//

#import "BaseVC.h"
#import <CoreLocation/CoreLocation.h>

@interface HomeVC : BaseVC <CLLocationManagerDelegate>
{
    
}

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property(nonatomic,weak)IBOutlet UILabel *lblName;

-(IBAction)onClickSignIn:(id)sender;
-(IBAction)onClickRegister:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblCopyRights;

@end
